v2.1.0 - 2023-08-30

- Backups are now compressed. They can be restored with the following command:

```bash
pg_restore -U <User> -d <Database> <Backup File Path>
```

v2.0.0 - 2023-03-22

- Removed support for deprecated installation procedure.
- Added `check` command to verify the integrity of the installation.
- Added `initversion` command to initialize the versioning system.
- Improve cross-platform support.