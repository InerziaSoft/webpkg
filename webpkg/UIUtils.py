import os

RED = '\033[31m'
GREEN = '\033[32m'
GRAY = '\033[90m'
RESET = '\033[0m'


class UIUtils:
    def __init__(self):
        pass

    @staticmethod
    def is_root():
        return os.geteuid() == 0

    @staticmethod
    def print_title(title):
        print(os.linesep, "\t=== ", title, " ===")

    @staticmethod
    def print_subtitle(subtitle):
        print("\t\t= ", subtitle, " =")

    @staticmethod
    def print_message(message):
        print(os.linesep, "\t** WARNING: ", message, " **", os.linesep)

    @staticmethod
    def print_dry_status(message):
        UIUtils.print_status("[DRY RUN]: " + message)

    @staticmethod
    def print_status(message):
        print("\t", GRAY, message, RESET, os.linesep)

    @staticmethod
    def print_check_result(succeeded, check):
        print("\t" + (GREEN if succeeded else RED) + (
            "V" if succeeded else "X") + " " + check + RESET + os.linesep)

    @staticmethod
    def print_error(error):
        print(os.linesep, RED, "\t** ERROR: ", error, " **", RESET, os.linesep)
