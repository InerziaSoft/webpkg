
class Git:
    def __init__(self, client):
        self.client = client

    def clone(self, repo, path, branch=None, single_branch=False):
        options = ""
        if branch is not None:
            options += " -b " + branch

        if single_branch:
            options += " --single-branch"

        self.client.execute("git clone " + repo + options + " " + path)

    def pull(self, path):
        self.client.execute_in_directory(path, ["git reset --hard", "git pull"])

    def fetch(self, path):
        self.client.execute_in_directory(path, ["git fetch"])

    def status(self, path):
        self.client.execute_in_directory(path, ["git status"])

    def config_submodules(self, path):
        self.client.execute(path, ["git submodule init", "git submodule update"])
