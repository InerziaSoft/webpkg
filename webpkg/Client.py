import os
import subprocess
from distutils.version import StrictVersion

from webpkg.Git import Git


# noinspection PyMethodMayBeStatic
class Client:
    def __init__(self):
        self.git = Git(self)

    def execute(self, command, sudo=False):
        subprocess.call(command, shell=True)
        return {"out": "", "err": ""}

    def execute_in_directory(self, path, commands):
        self.execute("cd " + path + " && " + "&& ".join(commands))

    # File Handling

    def file(self, filename, mode):
        return open(filename, mode)

    def put(self, local_filename, remote_filename, directory=False):
        command = "cp "
        if directory:
            command += "-r "

        self.execute(command + local_filename + " " + remote_filename)

    def remove(self, filename, recursive=False, force=False):
        command = "rm "
        if recursive:
            command += "-r "
        if force:
            command += "-f "

        command += filename
        self.execute(command)

    def make_dir(self, filename, parent=False):
        options = ""
        if parent:
            options += " -p "

        self.execute("mkdir " + options + filename)

    # noinspection PyMethodMayBeStatic
    def write_line_to_file(self, line, path):
        writefile = open(path, 'w')
        writefile.write(line)
        writefile.close()

    # noinspection PyMethodMayBeStatic
    def read_line_from_file(self, index, path):
        with open(path) as f:
            lines = f.readlines()

        return lines[index]

    def install_dependency(self, path, dependency):
        if dependency == "git":
            self.git.config_submodules(path)
        elif dependency == "composer":
            self.execute("cd " + path + " && composer install --no-ansi --no-dev --no-interaction "
                                        "--no-progress --no-scripts --optimize-autoloader")
        elif dependency == "bower":
            self.execute("cd " + path + " && bower install --allow-root --production")
        elif dependency == "npm":
            self.execute("cd " + path + " && npm install --production")
        else:
            raise ValueError("Unknown dependency type '" + dependency + "'!")

    # PostgreSQL
    def execute_script(self, database, script):
        self.put(script, "/tmp/install.sql")
        self.execute("psql -U '" + database + "' < /tmp/install.sql")

    # Database Versioning
    def versioning_file_path(self):
        return "/usr/local/share/webpkg/version.conf"

    def init_versioning(self):
        self.write_line_to_file("0.0.0", self.versioning_file_path())

    def versioning_allows(self, version):
        installed_version = StrictVersion(self.read_line_from_file(0, self.versioning_file_path()))
        available_version = StrictVersion(version)
        return available_version > installed_version

    def update_versioning(self, version):
        self.remove(self.versioning_file_path())
        self.write_line_to_file(version, self.versioning_file_path())

    def get_version(self):
        return self.read_line_from_file(0, self.versioning_file_path())
