# Release
All commits on `master` are automatically built and an artifact is produced. After you've ensured that the artifact works as expected, you can create a release. In order to do so, follow these steps:

- Update the version number in `webpkg.py` to the desired version.
- Update `CHANGELOG.md` with the changes since the last release.
- Commit the changes to `master` without pushing.
- Create a tag with the version number.
- Push all to GitLab.

The CI will take care of building and uploading the artifacts to a release. Once the release is published, use the associated artifact to get the `Job ID` and update the `README.md` file with the new link to download the latest version.