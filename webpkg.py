import json
import os
import argparse
import datetime
import sys

from webpkg.Client import Client
from webpkg.UIUtils import UIUtils

DEFAULT_WEB_PACKAGE_LOCATION = "/web"


def main():
    parser = argparse.ArgumentParser(
        prog="webpkg",
        description="Update and manage a Web package."
    )

    parser.add_argument(
        "command",
        action="store",
        choices=["fetch",
                 "update",
                 "dbupdate",
                 "backup",
                 "version",
                 "initversion",
                 "check"],
        help="A command to be executed."
    )

    parser.add_argument(
        "--root",
        action="store",
        help="Specify the folder where the web package is located",
        default=DEFAULT_WEB_PACKAGE_LOCATION
    )
    parser.add_argument(
        "-c",
        "--config",
        action="store",
        help="Specify a webpkg.json path relative to the root of the repository "
             "instead of using the default one.",
        default=None
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s 2.1.1 (2024-05-14)",
        help="Print the version of this program."
    )

    parser.add_argument(
        "--dry-run",
        dest="dry",
        action="store_true",
        help="Do not perform any action, just simulate and log.",
        default=False
    )

    arguments = parser.parse_args()

    root = os.path.abspath(arguments.root)
    UIUtils.print_status("Running in " + root)

    if arguments.command == "update":
        update(arguments.config, arguments.dry, root)
    elif arguments.command == "dbupdate":
        db_update(arguments.config, arguments.dry, root)
    elif arguments.command == "backup":
        backup(arguments.config, arguments.dry, root)
    elif arguments.command == "version":
        version(root)
    elif arguments.command == "initversion":
        init_version(arguments.dry)
    elif arguments.command == "fetch":
        fetch(root)
    elif arguments.command == "check":
        check(arguments.config, root)


def check(config=None, root=DEFAULT_WEB_PACKAGE_LOCATION):
    UIUtils.print_status("Starting check...")

    has_errors = False

    if os.path.exists(root):
        UIUtils.print_check_result(True, "Web package folder exists at " + root)
    else:
        has_errors = True
        UIUtils.print_check_result(False, "Web package folder doesn't exist at " + root + ". You can use '--root' to "
                                                                                          "change the default "
                                                                                          "location.")

    config_path = __get_config_path(root, config)
    if os.path.exists(config_path) and os.path.isfile(config_path):
        UIUtils.print_check_result(True, "Config JSON exists at " + config_path)

        try:
            configuration = __load_config(root, config)
            UIUtils.print_check_result(True, "Config JSON is valid.")

            backup_path = configuration["database"]["backup"]
            if os.path.exists(backup_path) and os.access(backup_path, os.W_OK):
                UIUtils.print_check_result(True, "Backup destination at " + backup_path + " exists and is writable.")
            else:
                UIUtils.print_check_result(False, "Backup destination at " + backup_path + " does not exist or is not "
                                                                                           "writable.")

        except Exception as e:
            UIUtils.print_check_result(False, f"Config JSON is invalid: {e}")
    else:
        has_errors = True
        UIUtils.print_check_result(False, "Config JSON not found at " + config_path + ". You can use '--config' to "
                                                                                      "specify the config relative "
                                                                                      "path from the root folder.")

    client = Client()
    versioning_path = client.versioning_file_path()
    if os.path.exists(versioning_path) and os.path.isfile(versioning_path):
        UIUtils.print_check_result(True, "Versioning file exists at " + versioning_path +
                                   " showing version " + client.get_version())
    else:
        has_errors = True
        UIUtils.print_check_result(False, "Versioning file doesn't exist at " + versioning_path + ". Use 'initversion"
                                                                                                  "' to create it.")

    if has_errors:
        UIUtils.print_error("Check failed.")
        sys.exit(-1)
    else:
        sys.exit(0)


def update(config=None, dry=False, root=DEFAULT_WEB_PACKAGE_LOCATION):
    client = Client()

    configDict = __load_config(root, config)

    if not dry:
        UIUtils.print_status("Pulling repository...")
        client.git.pull(root)
    else:
        UIUtils.print_dry_status("Attempting to pull repository in " + root + ". Fetching instead...")
        client.git.fetch()

    UIUtils.print_status("Configuring dependencies...")
    __handle_dependencies(client, root, configDict["dependencies"], dry)

    db_update(config, dry, root)


def db_update(config=None, dry=False, root=DEFAULT_WEB_PACKAGE_LOCATION):
    UIUtils.print_status("Performing backup...")
    backup(config, dry)

    client = Client()

    config = __load_config(root, config)

    UIUtils.print_status("Updating database...")
    __handle_exec_queue(client, root,
                        config["database"]["name"],
                        config["database"]["updates"],
                        config["update"],
                        dry)


def backup(config=None, dry=False, root=DEFAULT_WEB_PACKAGE_LOCATION):
    UIUtils.print_status("Initiating backup...")

    client = Client()

    config = __load_config(root, config)

    name = config["database"]["name"]
    path = os.path.join(config["database"]["backup"], str(datetime.datetime.today().strftime("%Y-%m-%d")) + ".bak")

    command = "pg_dump -U '" + name + "' -F c -b -f " + path + " '" + name + "'"

    if not dry:
        UIUtils.print_status("Backup started.")
        client.execute(command)
        UIUtils.print_status("Backup completed.")
    else:
        UIUtils.print_dry_status("Attempting to backup database '" + name + "' to '" + path + "'")
        UIUtils.print_dry_status(command)


def version(root=DEFAULT_WEB_PACKAGE_LOCATION):
    client = Client()
    UIUtils.print_title("Web package installed in " + root + " at version " + client.get_version() + ".")


def init_version(dry=False):
    client = Client()
    UIUtils.print_status("Checking whether versioning already exists...")

    if os.path.isfile(client.versioning_file_path()):
        if not dry:
            UIUtils.print_error(
                "Versioning is already configured! Refusing to overwrite. If you're experiencing issues, "
                "run 'check' to see what's wrong.")
            sys.exit(-1)
        else:
            UIUtils.print_status("Versioning is already configured: if you hadn't chose to dry run, it would have not "
                                 "been overridden and the command would have "
                                 "failed.")

    try:
        if not dry:
            client.init_versioning()
            UIUtils.print_status("Versioning initialized. Package is now at version " + client.get_version())
        else:
            UIUtils.print_status("Printing initial value for version file at " + client.versioning_file_path())

    except Exception as e:
        UIUtils.print_error(f"Unable to initialize the versioning system: {e}. Please, make sure that the \"webpkg\" "
                            f"folder at " + client.versioning_file_path() + " exists and is writable by this user.")
        sys.exit(-1)


def fetch(root=DEFAULT_WEB_PACKAGE_LOCATION):
    client = Client()
    UIUtils.print_status("Fetching Git repo...")

    client.git.fetch(root)
    client.git.status(root)


# Private Functions


def __database_update(client, database_sql, updates_folder):
    UIUtils.print_status("Scanning folder " + updates_folder + "...")

    if os.path.exists(updates_folder):
        for filename in os.listdir(updates_folder):
            sql = os.path.join(updates_folder, filename)
            UIUtils.print_status("Analyzing '" + sql + "'...")

            if os.path.isfile(sql):
                file_version = os.path.splitext(os.path.basename(sql))[0]
                UIUtils.print_status("Checking version '" + file_version + "'...")

                if client.versioning_allows(file_version):
                    UIUtils.print_status("Installing version '" + file_version + "'...")
                    client.execute_script(database_sql, sql)
                    client.update_versioning(file_version)
                else:
                    UIUtils.print_status("Version '" + file_version + "' is already installed.")
            else:
                UIUtils.print_error("File at path '" + sql + "' is invalid! Skipping...")
    else:
        UIUtils.print_status("No updates available.")


def __get_config_path(root, config_path):
    if config_path is None:
        config_path = "webpkg.json"

    return os.path.join(root, config_path)


def __load_config(folder, config):
    config = __get_config_path(folder, config)

    UIUtils.print_status("Reading config from '" + config + "'.")

    if os.path.isfile(config):
        with open(config, "r") as json_file:
            return json.load(json_file)
    else:
        UIUtils.print_error("No such file or directory: '" + config + "'!")
        sys.exit(-1)


def __handle_dependencies(client, folder, dependencies, dry):
    for dependency in dependencies:
        if not dry:
            try:
                client.install_dependency(folder, dependency)
            except ValueError as error:
                UIUtils.print_error(error)
        else:
            UIUtils.print_dry_status("Attempting to install dependencies for '" + dependency + "'.")


def __handle_exec_queue(client, folder, database, updates, queue, dry):
    for item in queue:
        if item["shell"] == "psql":
            script = os.path.join(folder, item["command"])
            if not dry:
                client.execute_script(database, script)
            else:
                UIUtils.print_dry_status("Executing script '" + script + "' on '" + database + "'.")
        elif item["shell"] == "bash" or item["shell"] == "sh":
            script = os.path.join(folder, item["command"])
            basepath = os.path.dirname(os.path.abspath(script))
            if not dry:
                client.execute_in_directory(basepath, ["eval " + script])
            else:
                UIUtils.print_dry_status("Executing shell script '" + script + "' in directory '" + basepath + "'.")
        elif item["shell"] == "internal" and item["command"] == "update":
            if not dry:
                __database_update(client, database, os.path.join(folder, updates))
            else:
                UIUtils.print_dry_status("Attempting to execute command '" +
                                         item["command"] + "' on the internal shell.")


if __name__ == "__main__":
    main()
