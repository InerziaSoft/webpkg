# WebPKG

WebPKG is a tool used to manage a web package. While it was originally created to manage a project living inside a Git repository, it is now more focused on managing database updates.

## Installation
To install WebPKG, you can use the following command:

```shell
curl -L -o webpkg.zip https://gitlab.com/InerziaSoft/webpkg/-/jobs/6851840672/artifacts/download && unzip webpkg.zip && sudo mv dist/webpkg.bin /usr/local/bin/webpkg && sudo chmod +x /usr/local/bin/webpkg && rm webpkg.zip && rm -rf dist
```

The command will install WebPKG under `/usr/local/bin/webpkg` and it'll be available in your `PATH` immediately.

## Configuration
In order for WebPKG to understand how a package works, you must define a `webpkg.json` file in the root of your project. An example configuration looks like this:

```json
{
  "update": [
    {
      "shell": "internal",
      "command": "update"
    }
  ],
  "database": {
    "name": "InerziaSoft",
    "updates": "db/updates",
    "backup": "/backup"
  }
}

```

This configuration instructs WebPKG that updates to this package should only trigger a database update sequence. The `database` section then tells WebPKG how the database is called and where to look for updates.

## `install`
WebPKG 2 has removed the ability to install a package. It is now recommended to either use CI/CD to deploy your package to the machine or to install manually.

## `check`
This command runs a high-level check of your setup to make sure WebPKG is ready to be used.

## `update` & `fetch`
WebPKG 2 continues to support packages that are deployed as Git repositories. For these packages, you can use the `update` command to perform a `git pull` operation.

This command will also take care of updating the dependencies, if they are defined in the `webpkg.json`:

```json
{
  "dependencies": [
    "composer",
    "bower",
    "npm",
    "git"
  ]
}
```

_Only the dependency mechanism shown above are supported._

After the update operation has completed successfully, WebPKG will also automatically invoke `dbupdate`.

## `dbupdate`
In order to manage progressive updates to the database schemas, web packages running with WebPKG can take advantage of the database versioning system.

The idea is very simple: each package provides a folder that contains SQL files named as the schema version. For example, starting from schema version `1.0.0`, the next update would be `1.0.1.sql` saved under `db/updates` (or the folder defined under `database > updates` in the `webpkg.json`).

When running the `update` (or just the `dbupdate`) command, WebPKG will proceed as follows:
- It will perform a backup of the current database _(see `backup` below for further info)._
- It will enumerate all the files in `db/updates` and gather the current version from `/usr/local/share/webpkg/version.conf`.
- It will then proceed to compare each file name as a version string against the current version: each file that will match as higher than the current version will be executed using `psql` (which must be installed and available in `PATH`).
- After each update, the `version.conf` file will be updated with the latest installed version.
- Once no more files are available, it will terminate successfully.

## `initversion`
As of WebPKG 2, it is assumed that you have installed the web package via CI/CD or with a previous version of WebPKG. If you're deploying a new instance, you will need to manually initialize the versioning system. You can do that by running `initversion`.

## `version`
You can check the currently installed version, which will correspond to the last schema update that has been installed or `0.0.0` if no updates have been installed yet.

This command will fail if the versioning system hasn't been configured yet.

## `backup`
WebPKG can automatically backup your database by running the `pg_dump` command. You can configure to run this command periodically by invoking `backup` with `cron` or `anacron`.

Backup files are automatically named as `<YEAR>-<MONTH>-<DAY>.sql` and their location is defined in the `webpkg.json` under `database > backup`.

> Note: in order for this command to work, the `backup` destination must be writable and the database must allow password-less access to the local PostgreSQL instance. You can turn this on by modifying your `pg_hba.conf` file to `trust` all local connections.